﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("task1");
            Task1 a = new Task1();
            Console.WriteLine("enter 2 numbers");
            int num1 = a.ParseAndValidateIntegerNumber(Console.ReadLine());
            int num2 = a.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine(a.Multiplication(num1, num2));



            Console.WriteLine("task2");
            Task2 s = new Task2();
            Console.WriteLine("enter a natural number");
            s.TryParseNaturalNumber(Console.ReadLine(), out int task2Num);
            foreach (int z in s.GetEvenNumbers(task2Num))
                Console.WriteLine(z);



            Console.WriteLine("task3");
            Task3 d = new Task3();
            Console.WriteLine("enter a natural number");
            d.TryParseNaturalNumber(Console.ReadLine(), out int task3Num);
            Console.WriteLine("enter a natural number");
            d.TryParseNaturalNumber(Console.ReadLine(), out int task3NumRemove);
            Console.WriteLine(d.RemoveDigitFromNumber(task3Num, task3NumRemove));           

        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            try
            {                
                return int.Parse(source);
            }
            catch
            {
                throw new ArgumentException("not an integer");
            }          
            
        }

        public int Multiplication(int num1, int num2)
        {            
            int sum = Enumerable.Range(0, Math.Abs(num2)).Select(n => num1).Sum();            
            if (num2 < 0) return -sum;
            else return sum;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {       

            for (int i=0;i<2;i++)
            {                
                if (int.TryParse(input, out result) && result >= 0) return true;
                else if(i==1)
                {
                    Console.WriteLine("enter a natural number");
                    input = Console.ReadLine();                                        
                }
            }
            result = -1;
            return false;

        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            return Enumerable.Range(0, naturalNumber * 2).Where(n => n % 2 == 0).ToList();        
           
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            for (int i = 0; i < 2; i++)
            {
                if (int.TryParse(input, out result) && result >= 0) return true;
                else if (i == 1)
                {
                    Console.WriteLine("enter a natural number");
                    input = Console.ReadLine();
                }
            }
            result = -1;
            return false;
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string strSource = source.ToString();            
            for (int index= strSource.IndexOf(digitToRemove.ToString()) ; strSource.IndexOf(digitToRemove.ToString()) != -1;)
            {
                strSource = strSource.Remove(index, 1);                
            }
            if(strSource=="-1") return "false";
            return strSource;
        }
    }
}
